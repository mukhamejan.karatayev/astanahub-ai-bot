import { UseChatHelpers } from 'ai/react'

import { Button } from '@/components/ui/button'
import { ExternalLink } from '@/components/external-link'
import { IconArrowRight } from '@/components/ui/icons'

const exampleMessages = [
  {
    heading: 'Как подать заявку на участие в Astana Hub?',
    message: `Как подать заявку на участие в Astana Hub?`
  },
  {
    heading: 'Кто является участником Astana Hub?',
    message: `Кто является участником Astana Hub?`
  },
  {
    heading: 'Что дает статус участника Astana Hub?',
    message: `Что дает статус участника Astana Hub?`
  },
]

export function EmptyScreen({ setInput }: Pick<UseChatHelpers, 'setInput'>) {
  return (
    <div className="mx-auto max-w-2xl px-4">
      <div className="rounded-lg border bg-background p-8">
        <h1 className="mb-2 text-lg font-semibold">
          Добро пожаловать в Astana Hub AI Chatbot!
        </h1>
        <p className="mb-2 leading-normal text-muted-foreground">
          Это умный чат-бот, который поможет вам найти ответы на ваши вопросы о Astana Hub.
        </p>
        <p className="leading-normal text-muted-foreground">
          {/* You can start a conversation here or try the following examples: */}
          Вы можете начать диалог или попробовать следующие примеры:
        </p>
        <div className="mt-4 flex flex-col items-start space-y-2">
          {exampleMessages.map((message, index) => (
            <Button
              key={index}
              variant="link"
              className="h-auto p-0 text-base"
              onClick={() => setInput(message.message)}
            >
              <IconArrowRight className="mr-2 text-muted-foreground" />
              {message.heading}
            </Button>
          ))}
        </div>
      </div>
    </div>
  )
}
